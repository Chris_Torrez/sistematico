/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import models.ManageFile;
import models.Producto;
import views.InternalShow;
import views.MainForm;

/**
 *
 * @author Christian
 */
public class MainController implements ActionListener {
    
    private MainForm frame;
    private JFileChooser chooser;
    
    public MainController(){
        frame = new MainForm();
        chooser = new JFileChooser();
    }
    
    public MainController(MainForm f){
        frame = f;
        chooser = new JFileChooser();
    }
    

    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Seleccionar":
                showOpenFileDialog();
                break;
            case "Limpiar":
                clean();
                break;
            case "guardar":
                showSaveFileDialog();
                break;
            default: System.out.println("entra al case");
            
        }
    }
    
     private File showOpenFileDialog(){
        File file = null;
        if (chooser.showOpenDialog(frame)==JFileChooser.APPROVE_OPTION){
            file = chooser.getSelectedFile();
            if(file!=null){
                try {
                    readFile(file);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
               JOptionPane.showMessageDialog(frame, "No se seleccionó archivo");
            }
        }
        return file;
    }
     
      private void readFile(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            ManageFile myCustomFile = (ManageFile) ois.readObject();
            myCustomFile.setFileName(file.getName());
            //JOptionPane.showMessageDialog(frame, myCustomFile.getContent());
            showFileInForm(myCustomFile);
        
    }
      
       private void showFileInForm(ManageFile customfile)
    {
            InternalShow n = new InternalShow(customfile);
            frame.desktop.add(n);
            n.setVisible(true);
            frame.desktop.getDesktopManager().maximizeFrame(n);
    }
       
        private void clean() {
        frame.boxNameProducto.setText("");
        frame.boxPrecioProducto.setText("");
        
    }
    
     private void showSaveFileDialog(){
       File file;
       if (chooser.showSaveDialog(frame)==JFileChooser.APPROVE_OPTION){
            file = chooser.getSelectedFile();
            if(file!=null){
                Producto producto = new Producto(1,frame.boxNameProducto.getText(),frame.boxPrecioProducto.getText());
                try {                    
                    writeFile(file, producto);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
               JOptionPane.showMessageDialog(frame, "No se seleccionó archivo", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }        
    }
     
     private void writeFile(File file, Producto producto) throws IOException {      
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(producto);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     
   
}
